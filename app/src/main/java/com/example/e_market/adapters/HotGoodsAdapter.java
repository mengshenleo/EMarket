package com.example.e_market.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.e_market.R;
import com.example.e_market.beans.GoodsBean;
import com.loopj.android.image.SmartImageView;

import java.io.File;
import java.util.ArrayList;

public class HotGoodsAdapter extends BaseAdapter {
    //需要适配的商品清单
    private ArrayList<GoodsBean> goodsBeanArrayList;
    private LayoutInflater layoutInflater;
    //构造方法
    public HotGoodsAdapter(Context context,ArrayList<GoodsBean> goodsBeanArrayList) {
        this.goodsBeanArrayList=goodsBeanArrayList;
        this.layoutInflater=LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return goodsBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return goodsBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //View view;
        //if(convertView == null) {}
        convertView = layoutInflater.inflate(R.layout.hot_goods_item, null);
        //view = LayoutInflater.from(context).inflate
        ImageView hot_goods_img = (SmartImageView) convertView.findViewById(R.id.hot_goods_img);
        TextView hot_goods_name = (TextView) convertView.findViewById(R.id.hot_goods_name);
        TextView hot_goods_price = (TextView) convertView.findViewById(R.id.hot_goods_price);
        GoodsBean goodsBean = goodsBeanArrayList.get(position);
        switch (goodsBean.getG_src()){
            case "g1.jpg": hot_goods_img.setImageResource(R.drawable.g1);break;
            case "g2.jpg": hot_goods_img.setImageResource(R.drawable.g2);break;
            case "g3.jpg": hot_goods_img.setImageResource(R.drawable.g3);break;
            case "g4.jpg": hot_goods_img.setImageResource(R.drawable.g4);break;
            case "g5.jpg": hot_goods_img.setImageResource(R.drawable.g5);break;
            case "g6.jpg": hot_goods_img.setImageResource(R.drawable.g6);break;
            case "g7.jpg": hot_goods_img.setImageResource(R.drawable.g7);break;
            case "g8.jpg": hot_goods_img.setImageResource(R.drawable.g8);break;
            case "g9.jpg": hot_goods_img.setImageResource(R.drawable.g9);break;
            case "g10.jpg": hot_goods_img.setImageResource(R.drawable.g10);break;
            case "g11.jpg": hot_goods_img.setImageResource(R.drawable.g11);break;
            case "g12.jpg": hot_goods_img.setImageResource(R.drawable.g12);break;
        }
        hot_goods_name.setText(goodsBean.getG_name());
        hot_goods_price.setText(String.valueOf(goodsBean.getG_price()));

        return convertView;
    }
    class ViewHolder{
        SmartImageView hot_goods_img;
        TextView hot_goods_name;
        TextView hot_goods_price;

    }
}
